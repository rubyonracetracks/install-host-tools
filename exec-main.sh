#!/bin/bash

# Update software list
echo '-------------------'
echo 'sudo apt-get update'
sudo apt-get update

echo '**********************************'
echo 'BEGIN installing development tools'
echo '**********************************'

# Preferred terminal for Linux
echo '----------------------------------'
echo 'sudo apt-get install -y lxterminal'
sudo apt-get install -y lxterminal

# Generates, encrypts, and saves passwords
echo '---------------------------------'
echo 'sudo apt-get install -y keepassxc'
sudo apt-get install -y keepassxc

# Editor
echo '-----------------------------'
echo 'sudo apt-get install -y geany'
sudo apt-get install -y geany

# Alternate browsers (good for seeing how universal your app is)
echo '--------------------------------'
echo 'sudo apt-get install -y palemoon'
sudo apt-get install -y palemoon

# Another Firefox alternative
echo '--------------------------------'
echo 'sudo apt-get install -y chromium'
sudo apt-get install -y chromium

# File search for Linux
echo '------------------------------------'
echo 'sudo apt-get install -y searchmonkey'
sudo apt-get install -y searchmonkey

# View SQLite database
echo '-------------------------------------'
echo 'sudo apt-get install -y sqlitebrowser'
sudo apt-get install -y sqlitebrowser

# View PostgreSQL database with Pgweb
echo '----------------'
echo 'Installing Pgweb'
git clone https://gitlab.com/rubyonracetracks/pgweb-debian-install.git
cd pgweb-debian-install && bash main.sh

# View .dot files
echo '----------------------------'
echo 'sudo apt-get install -y xdot'
sudo apt-get install -y xdot

echo '*************************************'
echo 'FINISHED installing development tools'
echo '*************************************'

echo '########################'
echo 'BEGIN adding web filters'
echo '########################'

echo '------------------------------------------------------'
echo 'git clone https://gitlab.com/jhsu802701/web_filter.git'
git clone https://gitlab.com/jhsu802701/web_filter.git

cd web_filter && bash main.sh
wait
rm -rf web_filter

echo '###########################'
echo 'FINISHED adding web filters'
echo '###########################'

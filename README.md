# Install Host Tools
This [Ruby on Racetracks](http://www.rubyonracetracks.com/) repository contains scripts for automatically installing software tools.

## Prerequisites
* Debian Stable or one of its derivatives (such as SparkyLinux, MX Linux, antiX Linus, or SolydXK) should be installed on your computer as the host OS or in a virtual machine.
* If you're using a different Linux distro, a Mac, or a Windows machine, you can still use this repository as a cheat sheet on things to do when setting up your system.
* Git should be installed in your Debian Stretch-based installation.  If you have not installed Git, you can install it by entering the following command in the terminal:

```
sudo apt-get update; sudo apt-get -y install git
```

## Procedure
Enter the following commands in the terminal:

```
cd
mkdir -p rubyonracetracks
cd rubyonracetracks
git clone https://gitlab.com/rubyonracetracks/install-host-tools.git
cd install-host-tools
bash main.sh
```
